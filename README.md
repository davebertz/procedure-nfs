# PRÉREQUIS POUR LA PROCÉDURE
- Accès en PR à IDM
- Avoir un projet OpenShift créé pour le client (dont il a accès)
- Avoir un compte de service dans IDM de PR pour le client et noter le UID du compte
- Être cluster admin sur OpenShift

# COMMANDES UTILES POUR OPENSHIFT
## Connexion au cluster openshift en mode CLI
oc login https://ul-pca-pr-vma01.l.ul.ca --insecure-skip-tls-verify
## Changer le projet courant
oc project nom_de_votre_projet
## Ajouter/modifier une composante openshift à partir d'un fichier YAML
oc apply -f nom_de_votre_fichier.yaml

# MISE EN PLACE D'OPENSHIFT (CLUSTER-ADMIN)
- Pour appliquer un fichier de type .yaml au project openshift, utiliser la commande suivante:
- Créer un compte de service pour le projet OpenShift du client. [Template](template/service-account.yaml)
- Créer un SCC qui oblige les pods à rouler sur le UID du compte de service créé dans IDM. [Template](template/scc.yaml)
- Appliquer le SCC au compte de service créé précédemment.
> oc adm policy add-scc-to-user nom_du_scc -z nom_du_service_account
- Créer un persistent volume de type NFS sur le serveur NFS. [Template](template/nfs-client-pv.yaml)
- Fournir au client: le nom du persistent volume, ainsi que le nom du service account qu'il doit utiliser dans son deployment config.

# CLIENT
- Créer votre persistent volume claim. [Template](template/nfs-client-pvc.yaml)
- Créer votre deployment config en spécifiant les volumes et le service account qu'on vous a fourni, ainsi que votre claim. [Template](template/nfs-client-dc.yaml)